import React, { Component } from "react";
import Input from "./Input";
import Button from "./Button";
import * as math from "mathjs";
import "../css/MainCalculator.css";

class MainCalculator extends Component {
  state = {
    value: "",
  };

  handleClick = (symbol) => {
    this.setState((prevState) => {
      return { value: prevState.value + symbol };
    });
  };

  handleClear = () => {
    this.setState({ value: "" });
  };

  showResult = () => {
    if (
      this.state.value.endsWith("+") ||
      this.state.value.endsWith("-") ||
      this.state.value.endsWith("*") ||
      this.state.value.endsWith("/")
    ) {
      this.setState((prevState) => {
        return { value: prevState.value };
      });
      return;
    }

    this.setState((prevState) => {
      return { value: prevState.value === "" ? "" : String(math.evaluate(prevState.value))};
    });
  };

  render() {
    return (
      <div className="calculator">
        <Input value={this.state.value} />
        <table>
          <tbody>
            <tr>
              <td>
                <Button value="7" handleClick={this.handleClick} />
              </td>
              <td>
                <Button value="8" handleClick={this.handleClick} />
              </td>
              <td>
                <Button value="9" handleClick={this.handleClick} />
              </td>
              <td style={{ backgroundColor: "#d12c4d" }}>
                <Button value="/" handleClick={this.handleClick} />
              </td>
            </tr>
            <tr>
              <td>
                <Button value="4" handleClick={this.handleClick} />
              </td>
              <td>
                <Button value="5" handleClick={this.handleClick} />
              </td>
              <td>
                <Button value="6" handleClick={this.handleClick} />
              </td>
              <td style={{ backgroundColor: "#d12c4d" }}>
                <Button value="*" handleClick={this.handleClick} />
              </td>
            </tr>
            <tr>
              <td>
                <Button value="1" handleClick={this.handleClick} />
              </td>
              <td>
                <Button value="2" handleClick={this.handleClick} />
              </td>
              <td>
                <Button value="3" handleClick={this.handleClick} />
              </td>
              <td style={{ backgroundColor: "#d12c4d" }}>
                <Button value="+" handleClick={this.handleClick} />
              </td>
            </tr>
            <tr>
              <td>
                <Button value="0" handleClick={this.handleClick} />
              </td>
              <td>
                <Button value="." handleClick={this.handleClick} />
              </td>
              <td style={{ backgroundColor: "#d12c4d" }}>
                <Button value="=" handleClick={this.showResult} />
              </td>
              <td style={{ backgroundColor: "#d12c4d" }}>
                <Button value="-" handleClick={this.handleClick} />
              </td>
            </tr>
            <tr style={{ backgroundColor: "#d12c4d" }}>
              <td colSpan={4}>
                <Button value="Clear" handleClick={this.handleClear} />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default MainCalculator;

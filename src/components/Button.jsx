import React, { Component } from "react";
import "../css/button.css";

class Button extends Component {
  render() {
    return (
      <div
        className="btn"
        onClick={() => this.props.handleClick(this.props.value)}
      >
        {this.props.value}
      </div>
    );
  }
}

export default React.memo(Button);
